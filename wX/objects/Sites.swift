// *****************************************************************************
// Copyright (c)  2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024 joshua.tee@gmail.com. All rights reserved.
//
// Refer to the COPYING file of the official project for license.
// *****************************************************************************

import Foundation

final class Sites {

    var sites: [Site] = []
    var byCode: [String: Site] = [:]
    var codeList: [String] = []
    var nameList: [String] = []
    
    var nameDict: [String: String] = [:]
    var latDict: [String: String] = [:]
    var lonDict: [String: String] = [:]
    
    init(_ nameDict: [String: String], _ latDict: [String: String], _ lonDict: [String: String], _ lonReversed: Bool = false) {
        self.nameDict = nameDict
        self.latDict = latDict
        self.lonDict = lonDict
        
        // checkValidityMaps()

        for (key, value) in nameDict {
            sites.append(Site(key, value, latDict[key]!, lonDict[key]!, lonReversed))
            byCode[key] = sites.last!
        }

        sites.sort(by: {$0.fullName < $1.fullName})

        for site in sites {
            codeList.append(site.codeName)
            nameList.append("\(site.codeName): \(site.fullName)")
        }
    }

    func checkValidityMaps() {
        let k1 = Set(nameDict.keys)
        let k2 = Set(latDict.keys)
        let k3 = Set(lonDict.keys)
        if k1 != k2 {
            print("mismatch between names and lat", k2.subtracting(k1), k1.subtracting(k2))
        }
        if k1 != k3 {
            print("mismatch between names and lon", k3.subtracting(k1), k1.subtracting(k3))
        }
    }

    func getNearest(_ latLon: LatLon) -> String {
        for site in sites {
            site.distance = Int(LatLon.distance(latLon, site.latLon))
        }
        sites.sort(by: {$0.distance < $1.distance})
        return sites[0].codeName
    }

    func getNearestSite(_ latLon: LatLon, _ order: Int = 0) -> Site {
        for site in sites {
            site.distance = Int(LatLon.distance(latLon, site.latLon))
        }
        sites.sort(by: {$0.distance < $1.distance})
        return sites[order]
    }

    // TODO FIXME
//    func getNearestList(_ latLon: LatLon, _ count: Int = 5) -> [String] {
//        for site in sites {
//            site.distance = Int(LatLon.distance(latLon, site.latLon))
//        }
//        sites.sort(by: {$0.distance < $1.distance})
//        return Array(sites[0...count])
//    }

    func getNearestInMiles(_ latLon: LatLon) -> Int {
        for site in sites {
            site.distance = Int(LatLon.distance(latLon, site.latLon))
        }
        sites.sort(by: {$0.distance < $1.distance})
        return sites[0].distance
    }
}
