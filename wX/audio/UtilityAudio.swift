// *****************************************************************************
// Copyright (c)  2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024 joshua.tee@gmail.com. All rights reserved.
//
// Refer to the COPYING file of the official project for license.
// *****************************************************************************

import UIKit
import AVFoundation

final class UtilityAudio {

    static func playClicked(_ string: String, _ synthesizer: AVSpeechSynthesizer, _ playButton: ToolbarIcon) {
        if synthesizer.isPaused {
            synthesizer.continueSpeaking()
            playButton.set(.pause)
        } else if !synthesizer.isSpeaking {
            let myUtterance = AVSpeechUtterance(string: UtilityTtsTranslations.translateAbbreviations(string))
            synthesizer.speak(myUtterance)
            playButton.set(.pause)
        } else {
            synthesizer.pauseSpeaking(at: AVSpeechBoundary.word)
            playButton.set(.play)
        }
    }

    static func playClicked(_ string: String, _ synthesizer: AVSpeechSynthesizer, _ fab: Fab) {
        if synthesizer.isPaused {
            synthesizer.continueSpeaking()
            fab.setImage(.pause)
        } else if !synthesizer.isSpeaking {
            let myUtterance = AVSpeechUtterance(string: UtilityTtsTranslations.translateAbbreviations(string))
            synthesizer.speak(myUtterance)
            fab.setImage(.pause)
        } else {
            synthesizer.pauseSpeaking(at: AVSpeechBoundary.word)
            fab.setImage(.play)
        }
    }

    static func playClickedNewItem(_ string: String, _ synthesizer: AVSpeechSynthesizer, _ fab: Fab) {
        let myUtterance = AVSpeechUtterance(string: UtilityTtsTranslations.translateAbbreviations(string))
        synthesizer.speak(myUtterance)
        fab.setImage(.pause)
    }

    static func resetAudio(_ uiv: UIwXViewControllerWithAudio) {
        if uiv.synthesizer.isSpeaking {
            uiv.synthesizer.pauseSpeaking(at: AVSpeechBoundary.word)
        }
        uiv.synthesizer = AVSpeechSynthesizer()
    }
}
