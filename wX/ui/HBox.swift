// *****************************************************************************
// Copyright (c)  2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024 joshua.tee@gmail.com. All rights reserved.
//
// Refer to the COPYING file of the official project for license.
// *****************************************************************************

import UIKit

final class HBox: Widget {

    private let uiStackView = UIStackView()
    
    // for a normal UIStackView:
    // default distribution is .fill
    // default aix is .horizontal
    // default spacing is 0.0

    init(
        _ distribution: UIStackView.Distribution,
        _ spacing: CGFloat = 0.0,
        _ arrangedSubviews: [Widget] = []
    ) {
        uiStackView.distribution = distribution
        uiStackView.axis = .horizontal
        uiStackView.spacing = spacing
        if !arrangedSubviews.isEmpty {
            arrangedSubviews.forEach {
                uiStackView.addArrangedSubview($0.getView())
            }
        }
    }

    func addWidget(_ w: UIView) {
        uiStackView.addArrangedSubview(w)
    }
    
    func addWidget(_ w: Widget) {
        uiStackView.addArrangedSubview(w.getView())
    }

    func addLayout(_ layout: Widget) {
        uiStackView.addArrangedSubview(layout.getView())
    }

    func get() -> UIStackView {
        uiStackView
    }
    
    func getView() -> UIView {
        uiStackView
    }
    
    func removeViews() {
        uiStackView.removeViews()
    }
    
    func removeChildren() {
        uiStackView.subviews.forEach {
            $0.removeFromSuperview()
        }
    }
    
    func removeAllChildren() {
        uiStackView.removeViews()
        uiStackView.removeFromSuperview()
    }
    
    func removeArrangedViews() {
        uiStackView.removeArrangedViews()
    }

    var isAccessibilityElement: Bool {
        get { uiStackView.isAccessibilityElement }
        set { uiStackView.isAccessibilityElement = newValue }
    }
    
    var isHidden: Bool {
        get { uiStackView.isHidden }
        set { uiStackView.isHidden = newValue }
    }
    
    var axis: NSLayoutConstraint.Axis {
        get { uiStackView.axis }
        set { uiStackView.axis = newValue }
    }
    
    var spacing: CGFloat {
        get { uiStackView.spacing }
        set { uiStackView.spacing = newValue }
    }

    var accessibilityLabel: String {
        get { uiStackView.accessibilityLabel ?? "" }
        set { uiStackView.accessibilityLabel = newValue }
    }

    var alignment: UIStackView.Alignment {
        get { uiStackView.alignment }
        set { uiStackView.alignment = newValue }
    }
    
    var widthAnchor: NSLayoutDimension { uiStackView.widthAnchor }
}
