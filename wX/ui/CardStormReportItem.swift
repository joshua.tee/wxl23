// *****************************************************************************
// Copyright (c)  2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024 joshua.tee@gmail.com. All rights reserved.
//
// Refer to the COPYING file of the official project for license.
// *****************************************************************************

import UIKit

final class CardStormReportItem {

    init(_ box: VBox, _ stormReport: StormReport, _ gesture: GestureData) {
        let tvLocation = TextLarge(80.0, "", ColorCompatibility.highlightText)
        let tvAddress = TextLarge(80.0)
        let tvDescription = TextSmallGray()
        if stormReport.damageHeader == "" && stormReport.time != "" {
            tvLocation.text = stormReport.state + ", " + stormReport.city + " " + stormReport.time
            tvAddress.text = stormReport.address
            tvDescription.text = stormReport.magnitude + " - " + stormReport.damageReport
            let boxV = VBox(0, [tvLocation, tvAddress, tvDescription])
            let card = Card(boxV)
            box.addLayout(card)
            card.connect(gesture)
        }
    }
}
