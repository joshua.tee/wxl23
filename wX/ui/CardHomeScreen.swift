// *****************************************************************************
// Copyright (c)  2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024 joshua.tee@gmail.com. All rights reserved.
//
// Refer to the COPYING file of the official project for license.
// *****************************************************************************

import UIKit

final class CardHomeScreen: Widget {
    
    private let uistackView = UIStackView()

    func setup() {
        uistackView.translatesAutoresizingMaskIntoConstraints = false
        uistackView.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        uistackView.axis = .vertical
        uistackView.alignment = .center
        uistackView.spacing = 0.0
    }
    
    func setup(_ stackView: UIStackView) {
        uistackView.widthAnchor.constraint(equalTo: stackView.widthAnchor).isActive = true
        uistackView.translatesAutoresizingMaskIntoConstraints = false
        uistackView.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        uistackView.axis = .vertical
        uistackView.alignment = .center
        uistackView.spacing = 0.0
    }

    func setupWithPadding() {
        setup()
        uistackView.spacing = UIPreferences.stackviewCardSpacing
    }
    
    func setupWithPadding(_ stackView: UIStackView) {
        setup(stackView)
        uistackView.spacing = UIPreferences.stackviewCardSpacing
    }
    
    func addWidget(_ w: UIView) {
        uistackView.addArrangedSubview(w)
    }

    func addLayout(_ w: Widget) {
        uistackView.addArrangedSubview(w.getView())
    }

    func connect(_ gesture: UIGestureRecognizer) {
        uistackView.addGestureRecognizer(gesture)
    }
    
    func removeFromSuperview() {
        uistackView.removeFromSuperview()
    }
    
    var widthAnchor: NSLayoutDimension { uistackView.widthAnchor }
    
    func get() -> UIStackView {
        uistackView
    }

    func getView() -> UIView {
        uistackView
    }
}
