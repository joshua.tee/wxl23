// *****************************************************************************
// Copyright (c)  2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024 joshua.tee@gmail.com. All rights reserved.
//
// Refer to the COPYING file of the official project for license.
// *****************************************************************************

import UIKit

final class CardBlackHeaderText {

    init(_ box: VBox, _ text: String) {
        let tvLocation = TextLarge(80.0, text, UIColor.blue)
        tvLocation.color = UIColor.white
        tvLocation.background = UIColor.black
        tvLocation.font = FontSize.extraLarge.size
        let vbox = VBox(0, [tvLocation])
        let card = Card(vbox)
        card.color = UIColor.black
        box.addLayout(card)
    }
}
