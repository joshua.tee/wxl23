// *****************************************************************************
// Copyright (c)  2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024 joshua.tee@gmail.com. All rights reserved.
//
// Refer to the COPYING file of the official project for license.
// *****************************************************************************

final class UtilityObservations {

    static let urls = [
        GlobalVariables.nwsWPCwebsitePrefix + "/sfc/sfcobs/large_latestsfc.gif",
        GlobalVariables.nwsWPCwebsitePrefix + "/sfc/namswsfcwbg.gif",
        GlobalVariables.nwsWPCwebsitePrefix + "/sfc/namscsfcwbg.gif",
        GlobalVariables.nwsWPCwebsitePrefix + "/sfc/namsesfcwbg.gif",
        GlobalVariables.nwsWPCwebsitePrefix + "/sfc/namcwsfcwbg.gif",
        GlobalVariables.nwsWPCwebsitePrefix + "/sfc/namccsfcwbg.gif",
        GlobalVariables.nwsWPCwebsitePrefix + "/sfc/namcesfcwbg.gif",
        GlobalVariables.nwsWPCwebsitePrefix + "/sfc/namnwsfcwbg.gif",
        GlobalVariables.nwsWPCwebsitePrefix + "/sfc/namncsfcwbg.gif",
        GlobalVariables.nwsWPCwebsitePrefix + "/sfc/namnesfcwbg.gif",
        GlobalVariables.nwsWPCwebsitePrefix + "/sfc/namaksfcwbg.gif",
        GlobalVariables.nwsWPCwebsitePrefix + "/sfc/namak2sfcwbg.gif",
        "https://ocean.weather.gov/UA/Conus.gif",
        "https://ocean.weather.gov/UA/West_coast.gif",
        "https://ocean.weather.gov/UA/USA_West.gif",
        "https://ocean.weather.gov/UA/USA_Mid_West.gif",
        "https://ocean.weather.gov/UA/USA_East.gif",
        "https://ocean.weather.gov/UA/East_coast.gif",
        "https://ocean.weather.gov/UA/Hawaii.gif",
        "https://ocean.weather.gov/UA/Alaska.gif",
        "https://ocean.weather.gov/UA/Canada.gif",
        "https://ocean.weather.gov/UA/USA_South.gif",
        "https://ocean.weather.gov/UA/Mexico.gif",
        "https://ocean.weather.gov/UA/OPC_PAC.gif",
        "https://ocean.weather.gov/UA/Pac_Tropics.gif",
        "https://ocean.weather.gov/UA/Pac_Difax.gif",
        "https://ocean.weather.gov/UA/OPC_ATL.gif",
        "https://ocean.weather.gov/UA/Atl_Tropics.gif",
        "https://ocean.weather.gov/UA/Atl_Difax.gif"
    ]

    static let labels = [
        "CONUS Surface Obs",
        "SW Surface Analysis",
        "SC Surface Analysis",
        "SE Surface Analysis",
        "CW Surface Analysis",
        "C Surface Analysis",
        "CE Surface Analysis",
        "NW Surface Analysis",
        "NC Surface Analysis",
        "NE Surface Analysis",
        "AK Surface Analysis",
        "Gulf of AK Surface Analysis",
        "UA - Continental USA",
        "UA - West Coast",
        "UA - USA West",
        "UA - USA Mid West",
        "UA - USA East",
        "UA - East Coast",
        "UA - Hawaii",
        "UA - Alaska",
        "UA - Canada",
        "UA - USA South",
        "UA - Gulf of Mexico",
        "UA - Pacific Ocean",
        "UA - Pacific Tropical",
        "UA - Pacific Ocean Difax",
        "UA - Atlantic Ocean",
        "UA - Atlantic Tropical",
        "UA - Atlantic Ocean Difax"
    ]
}
