// *****************************************************************************
// Copyright (c)  2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024 joshua.tee@gmail.com. All rights reserved.
//
// Refer to the COPYING file of the official project for license.
// *****************************************************************************

import UIKit

final class VcSpotterReports: UIwXViewController {

    private var spotterReportsData = [SpotterReports]()
    private var spotterReportsDataSorted = [SpotterReports]()
    private var spotterReportCountButton = ToolbarIcon()

    override func viewDidLoad() {
        super.viewDidLoad()
        spotterReportCountButton = ToolbarIcon(self, nil)
        spotterReportCountButton.title = ""
        toolbar.items = ToolbarItems([doneButton, GlobalVariables.flexBarButton, spotterReportCountButton]).items
        objScrollStackView = ScrollStackView(self)
        boxMain.constrain(self)
        getContent()
    }

    override func getContent() {
        spotterReportsData.removeAll()
        _ = FutureVoid({ self.spotterReportsData = UtilitySpotter.reportsList }, display)
    }

    func display() {
        boxMain.removeChildren()
        spotterReportCountButton.title = "Count: " + String(spotterReportsData.count)
        spotterReportsDataSorted = spotterReportsData.sorted { $1.time > $0.time }
        spotterReportsDataSorted.enumerated().forEach { index, item in
            _ = CardSpotterReport(boxMain, item, GestureData(index, self, #selector(buttonPressed)))
        }
        if spotterReportsData.count == 0 {
            let text = TextLarge(10.0, "No active spotter reports.", ColorCompatibility.highlightText)
            boxMain.addWidget(text)
        }
    }

    @objc func buttonPressed(sender: GestureData) {
        let index = sender.data
        let popUp = PopUp(self, "", spotterReportCountButton)
        popUp.add(Action("Show on map") { self.showMap(index) })
        popUp.finish()
    }

    func showMap(_ selection: Int) {
        Route.map(self, spotterReportsDataSorted[selection].location.latString, spotterReportsDataSorted[selection].location.lonString)
    }
}
