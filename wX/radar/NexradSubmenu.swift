// *****************************************************************************
// Copyright (c)  2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024 joshua.tee@gmail.com. All rights reserved.
//
// Refer to the COPYING file of the official project for license.
// *****************************************************************************

import UIKit

final class NexradSubmenu {

    let toolbar = Toolbar()
    var doneButton = ToolbarIcon()
    var timeButton = ToolbarIcon()
    var warningButton = ToolbarIcon()
    var radarSiteButton = ToolbarIcon()
    var productButton = [ToolbarIcon]()
    var animateButton = ToolbarIcon()
    var siteButton = [ToolbarIcon]()
    let nexradState: NexradState

    init(_ nexradState: NexradState) {
        self.nexradState = nexradState
    }

    func setupToolbar(_ uiv: UIViewController, _ toolbarTop: Toolbar, _ radarSiteClicked: Selector) {
        if !RadarPreferences.dualpaneshareposn && nexradState.numberOfPanes > 1 {
            uiv.view.addSubview(toolbarTop)
            toolbarTop.setConfigWithUiv(uiv, toolbarType: .top)
            nexradState.paneRange.forEach { index in
                siteButton.append(ToolbarIcon("L", uiv, radarSiteClicked, tag: index))
            }
            var items = [UIBarButtonItem]()
            items.append(GlobalVariables.flexBarButton)
            nexradState.paneRange.forEach { index in
                items.append(siteButton[index])
            }
            toolbarTop.items = ToolbarItems(items).items
            if UIPreferences.radarToolbarTransparent {
                toolbarTop.setTransparent()
            }
        }
        uiv.view.addSubview(toolbar)
        toolbar.setConfigWithUiv(uiv)
        if UIPreferences.radarToolbarTransparent {
            toolbar.setTransparent()
        }
    }

    func setupButtons(_ uiv: UIViewController,
                      _ selectordoneClicked: Selector,
                      _ selectorproductClicked: Selector,
                      _ selectorradarSiteClicked: Selector,
                      _ selectoranimateClicked: Selector,
                      _ selectortimeClicked: Selector,
                      _ selectorwarningClicked: Selector

    ) {
        doneButton = ToolbarIcon(uiv, .done, selectordoneClicked)
        nexradState.paneRange.forEach { index in
            productButton.append(ToolbarIcon("", uiv, selectorproductClicked, tag: index))
        }
        radarSiteButton = ToolbarIcon("", uiv, selectorradarSiteClicked)
        animateButton = ToolbarIcon(uiv, .play, selectoranimateClicked)
        var toolbarButtons = [UIBarButtonItem]()
        toolbarButtons.append(doneButton)
        if nexradState.numberOfPanes == 1 {
            timeButton = ToolbarIcon("", uiv, selectortimeClicked)
            warningButton = ToolbarIcon("", uiv, selectorwarningClicked)
            toolbarButtons.append(timeButton)
            toolbarButtons.append(warningButton)
        } else {
            warningButton = ToolbarIcon("", uiv, selectorwarningClicked)
            toolbarButtons.append(timeButton)
            toolbarButtons.append(warningButton)
        }
        toolbarButtons += [GlobalVariables.flexBarButton, animateButton, GlobalVariables.fixedSpace]
        nexradState.paneRange.forEach {
            toolbarButtons.append(productButton[$0])
        }
        if RadarPreferences.dualpaneshareposn || nexradState.numberOfPanes == 1 {
            toolbarButtons.append(radarSiteButton)
        }
        toolbar.items = ToolbarItems(toolbarButtons).items
    }

    func updateWarningsInToolbar() {
        if RadarPreferences.warnings {
            let tstCount = Warnings.getCount(PolygonEnum.TST)
            let torCount = Warnings.getCount(PolygonEnum.TOR)
            let ffwCount = Warnings.getCount(PolygonEnum.FFW)
            let countString = "(" + torCount + "," + tstCount + "," + ffwCount + ")"
            warningButton.title = countString
        }
    }
}
