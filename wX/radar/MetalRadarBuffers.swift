// *****************************************************************************
// Copyright (c)  2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024 joshua.tee@gmail.com. All rights reserved.
//
// Refer to the COPYING file of the official project for license.
// *****************************************************************************

final class MetalRadarBuffers: MetalBuffers {

    var bgColor = 0
    var fileName = "nids"
    var levelData = NexradLevelData()
    var fileStorage = FileStorage()
    var numberOfRadials = 0
    var numberOfRangeBins = 0
    var binSize = 0.0

    init(_ bgColor: Int) {
        self.bgColor = bgColor
    }

    var colorMap: ColorPalette { ColorPalette.colorMap[Int(levelData.productCode)]! }

    func initialize() {
        if !RadarPreferences.showRadarWhenPan {
            honorDisplayHold = true
        }
        if levelData.productCode == 37 || levelData.productCode == 38 || levelData.productCode == 41 || levelData.productCode == 57 {
            if floatBuffer.capacity < (48 * 464 * 464) {
                floatBuffer = MemoryBuffer(48 * 464 * 464)
            }
        } else {
            if floatBuffer.capacity < (32 * levelData.numberOfRadials * levelData.numberOfRangeBins) {
                floatBuffer = MemoryBuffer(32 * levelData.numberOfRadials * levelData.numberOfRangeBins)
            }
        }
        setToPositionZero()
    }
    
    func putColorsByIndex(_ level: UInt8) {
        putColor(colorMap.redValues.get(Int(level)))
        putColor(colorMap.greenValues.get(Int(level)))
        putColor(colorMap.blueValues.get(Int(level)))
    }
    
    func generateRadials() -> Int {
        let totalBins: Int
        switch levelData.productCode {
        case 37, 38:
            totalBins = NexradRaster.create(self)
        case 153, 154, 30, 56, 78, 80, 181:
            totalBins = NexradDecodeEightBit.createRadials(self)
        case 0:
            totalBins = 0
        default:
            totalBins = NexradDecodeEightBit.andCreateRadials(self, fileStorage)
        }
        return totalBins
    }
    
    func setCount() {
        count = (metalBuffer.count / floatCountPerVertex) * 2
    }
}
