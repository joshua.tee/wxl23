// *****************************************************************************
// Copyright (c)  2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024 joshua.tee@gmail.com. All rights reserved.
//
// Refer to the COPYING file of the official project for license.
// *****************************************************************************

enum RadarGeometryTypeEnum {
    case CaLines
    case MxLines
    case StateLines
    case CountyLines
    case HwLines
    case HwExtLines
    case LakeLines
    case NONE
}
