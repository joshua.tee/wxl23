// *****************************************************************************
// Copyright (c)  2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024 joshua.tee@gmail.com. All rights reserved.
//
// Refer to the COPYING file of the official project for license.
// *****************************************************************************

final class ObjectLocation {

    let lat: String
    let lon: String
    let name: String
    let wfo: String
    let rid: String
    let state: String
    let isLocationUS: Bool
    var observation: String
    private let prefNumberString: String

    init(_ locNumAsInt: Int) {
        let locNumAsString = String(locNumAsInt + 1)
        prefNumberString = locNumAsString
        lat = Utility.readPref("LOC" + locNumAsString + "_X", "")
        lon = Utility.readPref("LOC" + locNumAsString + "_Y", "")
        name = Utility.readPref("LOC" + locNumAsString + "_LABEL", "")
        wfo = Utility.readPref("NWS" + locNumAsString, "")
        rid = Utility.readPref("RID" + locNumAsString, "")
        state = RadarSites.getName(rid).split(",")[0]
        observation = Utility.readPref("LOC" + locNumAsString + "_OBSERVATION", "")
        isLocationUS = Location.us(lat)
        Location.addToListOfNames(name)
    }

    func saveToNewSlot(_ newLocNumInt: Int) {
        let locNumAsString = String(newLocNumInt + 1)
        Utility.writePref("LOC" + locNumAsString + "_X", lat)
        Utility.writePref("LOC" + locNumAsString + "_Y", lon)
        Utility.writePref("LOC" + locNumAsString + "_LABEL", name)
        Utility.writePref("NWS" + locNumAsString, wfo)
        Utility.writePref("RID" + locNumAsString, rid)
        Utility.writePref("LOC" + locNumAsString + "_OBSERVATION", observation)
        Location.refresh()
    }

    func updateObservation(_ observation: String) {
        self.observation = observation
        Utility.writePref("LOC" + prefNumberString + "_OBSERVATION", observation)
    }
}
