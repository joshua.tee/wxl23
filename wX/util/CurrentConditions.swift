// *****************************************************************************
// Copyright (c)  2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024 joshua.tee@gmail.com. All rights reserved.
//
// Refer to the COPYING file of the official project for license.
// *****************************************************************************

final class CurrentConditions {

    var data = ""
    var iconUrl = ""
    private var conditionsTimeString = ""
    var status = ""
    var topLine = ""
    var middleLine = ""
    private var temperature = ""
    private var windChill = ""
    private var heatIndex = ""
    private var dewPoint = ""
    private var relativeHumidity = ""
    private var seaLevelPressure = ""
    private var windDirection = ""
    private var windSpeed = ""
    private var windGust = ""
    private var windGustSpoken = ""
    private var visibility = ""
    private var condition = ""
    var spokenText = ""
    var timeStringUtc = ""
    var latLon = LatLon()
    var isUS = true

    func process(_ latLon: LatLon, _ index: Int = 0) {
        self.latLon = latLon
        var s = ""
        let objectMetar = ObjectMetar(latLon, index)
        conditionsTimeString = objectMetar.conditionsTimeString
        temperature = objectMetar.temperature + GlobalVariables.degreeSymbol
        windChill = objectMetar.windChill + GlobalVariables.degreeSymbol
        heatIndex = objectMetar.heatIndex + GlobalVariables.degreeSymbol
        dewPoint = objectMetar.dewPoint + GlobalVariables.degreeSymbol
        relativeHumidity = objectMetar.relativeHumidity + "%"
        seaLevelPressure = objectMetar.seaLevelPressure
        windDirection = objectMetar.windDirection
        windSpeed = objectMetar.windSpeed
        windGust = objectMetar.windGust
        visibility = objectMetar.visibility
        condition = objectMetar.condition
        timeStringUtc = objectMetar.timeStringUtc
        s += temperature
        if objectMetar.windChill != "NA" {
            s += "(" + windChill + ")"
        } else if objectMetar.heatIndex != "NA" {
            s += "(" + heatIndex + ")"
        }
        s += " / " + dewPoint + "(" + relativeHumidity + ")" + " - "
        s += seaLevelPressure + " - " + windDirection + " " + windSpeed
        if windGust != "" {
            s += " G "
            windGustSpoken = " gusting to " + windGust + " miles per hour "
        } else {
            windGustSpoken = ""
        }
        s += windGust + " mph" + " - " + visibility + " mi - " + condition

        data = s
        iconUrl = objectMetar.icon
        status = conditionsTimeString + " " + getObsFullName(objectMetar.obsClosest.codeName)
        formatCurrentConditions()

        // return [s, objectMetar.icon]
        // sb String "NA° / 22°(NA%) - 1016 mb - W 13 mph - 10 mi - Mostly Cloudy"
    }

    private func formatCurrentConditions() {
        let separator = " - "
        let dataList = data.split(separator)
        var topLineLocal = ""
        var middleLineLocal = ""
        if dataList.count > 4 {
            let list = dataList[0].split("/")
            topLineLocal = dataList[4].replaceAll("^ ", "") + " " + list[0] + dataList[2]
            middleLineLocal = list[1].replaceAll("^ ", "") + separator + dataList[1] + separator + dataList[3] + GlobalVariables.newline
            middleLineLocal += status
        }
        topLine = topLineLocal
        middleLine = middleLineLocal
        spokenText = condition + ", temperature is " + temperature + " with wind at " + UtilityLocationFragment.windCodeToSpoken(windDirection) + " "
            + windSpeed + " miles per hour " + windGustSpoken +
            " dew point is " + dewPoint + ", relative humidity is "
            + relativeHumidity + ", pressure in milli-bars is "
            + seaLevelPressure + ", visibility is " + visibility + " miles " + status
    }

    private func getObsFullName(_ obsSite: String) -> String {
        let locationName = Metar.sites.byCode[obsSite]!.fullName
        return locationName.trimnl() + " (" + obsSite + ") "
    }

    func timeCheck() {
        if isUS {
            let obsTime = ObjectDateTime.fromObs(timeStringUtc)
            let currentTime = ObjectDateTime.getCurrentTimeInUTC()
            let isTimeCurrent = ObjectDateTime.timeDifference(currentTime, obsTime.get(), 120)
            if !isTimeCurrent {
                 process(latLon, 1)
            }
        }
    }
}
